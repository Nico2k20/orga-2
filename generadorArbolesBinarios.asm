extern malloc
extern printf
extern free
section .data

nVacio db " {}",0
verNodo db " { %d", 0
cierreNodo db "}", 0

msj db "Es puntero"
len equ $ - msj


section .bss


section .text

global crearArbolB
global mostrarNodos
global eliminarTodos
global  buscarMin

crearArbolB:
    push ebp                    
    mov ebp, esp     
   
    
    mov ecx,[ebp + 16] 
    mov edx, [ebp + 12]  
    mov ebx, [ebp + 8 ]  
   
    cmp ecx,0
    je agregarNodo


    cmp edx,0
    je agregarNodo


    cmp dword[ecx],ebx
    jg mayor


    push ebx; 
    mov eax, dword[ecx + 4]  
    push eax
    call crearArbolB

mayor:
    push ebx
    mov eax, dword[ecx + 4]    
    push eax
    call crearArbolB


fin:

    mov esp, ebp
    pop ebp

    ret




agregarNodo:
       push ebx
       push 12
       call malloc
       add esp, 4 
       pop ebx
    
       mov [eax], ebx 
       mov ebx, 0
       mov [eax + 4], ebx
       mov [eax + 8], ebx
       jmp fin
 


mostrarNodos:
    push ebp 
    mov ebp, esp 
    mov ebx, [ebp + 8]
    cmp ebx, 0 
    je nodoVacio
    call nodoInicial
    push ebx
    mov eax, [ebx +4]
    push eax
    call mostrarNodos
    add esp, 4
    pop ebx
    mov eax, [ebx +8]
    push eax
    call mostrarNodos
    call nodoFinal
    jmp fin


nodoVacio:
    push eax
    push ebx
    push edx
    push nVacio
    call printf
    add esp, 4
    pop edx
    pop ebx
    pop edx
    jmp fin

nodoInicial:
    push eax
    push ebx
    push edx
    mov eax, [ebx]
    push eax
    push verNodo
    call printf
    add esp, 8
    pop edx
    pop ebx
    pop edx
    ret

nodoFinal:
    push eax
    push ebx
    push edx
    push cierreNodo
    call printf
    add esp, 4
    pop edx
    pop ebx
    pop edx
    ret


eliminarTodos:
    push ebp 
    mov ebp, esp 
    
    mov edx, [ebp + 12]
    mov ebx, [ebp + 8]
    cmp ebx, 0 
    je fin 
  
    
    cmp edx, [ebx]
    jg borrarALaDerecha
    jl borrarALaIzq
    jmp nodoEncontrado
    
    nodoEncontrado:
    push ebx
    call borrarArbol	
    add esp, 4
    jmp fin



    borrarArbol:
    push ebp 
    mov ebp, esp 
    
    mov ebx, [ebp + 8]
    cmp ebx, 0 
    je fin
    push ebx
    mov eax, [ebx + 4]
    push eax
    call borrarArbol	
    add esp, 4
    pop ebx
    push ebx 
    mov eax, [ebx + 8]
    push eax
    call borrarArbol	
    add esp, 4
    pop ebx
    push ebx
    call free 
    add esp, 4 
    jmp fin



    borrarALaIzq:
    push edx
    mov eax, [ebx + 4]
    push eax
    call eliminarTodos
    mov ebx, [ebp + 8]
    mov [ebx + 4], eax
    mov eax,ebx
    add esp, 8 
    jmp fin
    
    borrarALaDerecha:
    push edx; 
    mov eax, [ebx + 8]
    push eax
    call eliminarTodos
    mov ebx, [ebp + 8]
    mov [ebx + 8], eax
    mov eax,ebx
    add esp, 8 
    jmp fin



    buscarMin:
    push ebp 
    mov ebp, esp

    mov ebx, [ebp + 8]
    mov eax, [ebx]


    cmp ebx,0
    jmp fin

    cmp eax,[ebx]
    jg cambiarMinimo


    mov ecx, [ebx+4]
    mov [ebp+8],ecx
    
    call buscarMin

    jmp fin

cambiarMinimo:
    mov eax,ebx
    call buscarMin


