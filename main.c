#include <stdio.h>
#include <stdlib.h>




struct arbol {
   int  valor;
   struct arbol *izq;
   struct arbol *der;
   
};

struct arbol* crearArbolB_1(int valor);

struct arbol* eliminarTodos(struct arbol* arbol, int valor);

struct arbol* crearArbolB(int valor,struct arbol *izq,struct arbol *der);

struct arbol* buscarMin(struct arbol *arbolRaiz);

int suma(int a ,int b,int c);


void mostrarNodos(struct arbol *arb);


 

int main(int argc, char *argv[]){
 
   
    int valorInicial, tamanioArbol, contador, valorNuevo; 
    
    struct arbol arbolito,*apuntadorArbolIzq,*apuntadorArbolDer,*nodoAnterior;

    printf("Ingrese el tamaño del arbol: ");

    scanf("%d", &tamanioArbol);


    contador = tamanioArbol;
 
    
    printf("Ingrese valor inicial: ");

    scanf("%d", &valorInicial);

    
    arbolito.valor =  valorInicial;
    (arbolito.izq) =  0;
    (arbolito.der) =  0;      
     
    apuntadorArbolIzq = arbolito.izq; 
    apuntadorArbolDer = arbolito.der;

    int primeroIzq = 1;
    int primeroDer= 1;

    nodoAnterior = &arbolito;

    while (contador != 1){

        printf("Ingrese un numero: ");

        scanf("%d", &valorNuevo);
        
        if(valorNuevo>valorInicial) {
                if(primeroDer==1){
                        printf("Entre a la rama derecha\n");
                        arbolito.der = crearArbolB(valorNuevo,arbolito.izq,arbolito.der);
                        printf("Se ingreso al nodo derecho el valor  : %d\n",arbolito.der->valor);
                        nodoAnterior = arbolito.der;
                        primeroDer = 0;
                }
                else{
                        printf("Entre a la rama derecha\n");
                        apuntadorArbolDer = crearArbolB(valorNuevo,arbolito.izq,arbolito.der);
                        nodoAnterior -> der = apuntadorArbolDer;
                        printf("Se ingreso al nodo derecho el valor  : %d\n",(apuntadorArbolDer)->valor);
                }
               
                
        }


        if(valorNuevo<valorInicial){ 
                if(primeroIzq==1){
                        arbolito.izq = crearArbolB(valorNuevo,arbolito.izq,arbolito.der);
                        printf("Se ingreso al nodo izquierdo el valor : %d\n", arbolito.izq->valor);
                        nodoAnterior = arbolito.izq;
                        primeroIzq = 0;
                }
                else{
                        apuntadorArbolIzq = crearArbolB(valorNuevo,arbolito.izq,arbolito.der);
                        nodoAnterior->izq = apuntadorArbolIzq;
                        printf("Se ingreso al nodo izquierdo el valor : %d\n",(apuntadorArbolIzq)->valor);
                }
                
        } 
        contador--;
}


   

    mostrarNodos(&arbolito);


    eliminarTodos(&arbolito, 5);


    mostrarNodos(&arbolito);

    printf("\n  el minimo %p\n",buscarMin(&arbolito));

    ////////////////////////////////////////////////////////////////////////////////////////////

    return 0;
}


